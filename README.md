# Manage your analyses workflows with the `drake` R package

Slides for my presentation of the [drake](https://github.com/ropensci/drake)
R package at the
[Grenoble R users group](https://r-in-grenoble.github.io/), on 2018/12/06.

Inspired by the work of [Will Landau](https://github.com/wlandau) and
tutorial from [Kirill Müller](https://github.com/krlmlr)
([drake pitch](https://github.com/krlmlr/drake-pitch)). Thanks to them.
